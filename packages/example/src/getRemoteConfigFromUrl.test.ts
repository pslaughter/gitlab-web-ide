import { getRemoteConfigFromUrl } from './getRemoteConfigFromUrl';

describe('getRemoteConfigFromUrl', () => {
  it.each([
    {
      scenario: 'no remote development search params',
      searchParams: '',
      expected: undefined,
    },
    {
      scenario: 'remoteHost parameter',
      searchParams: '?remoteHost=localhost:9888',
      expected: {
        type: 'remote',
        config: { remoteAuthority: 'localhost:9888', hostPath: '', connectionToken: '' },
      },
    },
    {
      scenario: 'hostPath parameter',
      searchParams: '?hostPath=/path/to/project',
      expected: {
        type: 'remote',
        config: { remoteAuthority: '', hostPath: '/path/to/project', connectionToken: '' },
      },
    },
    {
      scenario: 'all parameters',
      searchParams: '?remoteHost=localhost:9888&hostPath=/path/to/project',
      expected: {
        type: 'remote',
        config: {
          remoteAuthority: 'localhost:9888',
          hostPath: '/path/to/project',
          connectionToken: '',
        },
      },
    },
  ])('works for $scenario', ({ searchParams, expected }) => {
    const result = getRemoteConfigFromUrl(searchParams);
    expect(result).toEqual(expected);
  });
});
