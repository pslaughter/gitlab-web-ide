import { BaseConfig, ClientOnlyConfig, RemoteConfig, AnyConfig } from '@gitlab/web-ide-types';

export const createBaseConfig = (): BaseConfig => ({
  baseUrl: 'foo.bar',
  handleError: jest.fn(),
  handleClose: jest.fn(),
  handleStartRemote: jest.fn(),
  handleTracking: jest.fn(),
  links: {
    feedbackIssue: 'foobar',
    userPreferences: 'user/preferences',
  },
});

export const createClientOnlyConfig = (): ClientOnlyConfig => ({
  ...createBaseConfig(),
  gitlabUrl: 'gitlab.com',
  gitlabToken: 'token',
  projectPath: 'gitlab-org/gitlab',
  ref: 'main',
});

export const remoteHostConfig = (): RemoteConfig => ({
  ...createBaseConfig(),
  remoteAuthority: 'foo.bar',
  hostPath: '/',
  connectionToken: 'foobar',
});

export const createFullConfig = (): AnyConfig => ({
  ...createBaseConfig(),
  ...createClientOnlyConfig(),
  ...remoteHostConfig(),
});
