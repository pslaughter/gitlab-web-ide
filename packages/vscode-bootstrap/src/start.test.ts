import { ClientOnlyConfig, ErrorType, RemoteConfig } from '@gitlab/web-ide-types';
import webIdeExtensionMeta from '@gitlab/web-ide-vscode-extension/vscode.package.json';
import { postMessage } from '@gitlab/vscode-mediator-commands';
import { useMockAMDEnvironment } from '../test-utils/amd';
import { startClientOnly, startRemote } from './start';
import { WorkbenchModule } from './vscode';

const baseConfig = {
  baseUrl: 'https://test.example.com',
  links: {
    feedbackIssue: 'foobar',
    userPreferences: 'user/preferences',
  },
};

const CLIENT_ONLY_TEST_CONFIG: ClientOnlyConfig = {
  ...baseConfig,
  gitlabToken: 'secrettoken',
  gitlabUrl: 'https://test.example-gitlab.com',
  projectPath: 'my-group/lorem',
  ref: 'abcdef123',
};

const REMOTE_TEST_CONFIG: RemoteConfig = {
  ...baseConfig,
  connectionToken: 'secrettoken',
  remoteAuthority: 'localhost:9888',
  hostPath: '/root/project',
};

jest.mock('@gitlab/vscode-mediator-commands');

describe('vscode-bootstrap start', () => {
  const amd = useMockAMDEnvironment();

  const workbenchDisposeSpy = jest.fn();
  const workbenchModule = {
    create: jest.fn(),
    events: {
      onWillShutdown: jest.fn(),
    },
    URI: {
      // TODO: Chad "This is weird"
      parse: (x: string) => `URI.parse-${x}`,
      from: (x: Record<string, string>) => `URI.from-${x.scheme}-${x.path}-${x.authority}}`,
    },
    logger: {
      log: jest.fn(),
    },
  };
  const bufferModule = {};

  const triggerOnWillShutdown = () => {
    const [handler] = workbenchModule.events.onWillShutdown.mock.calls[0];

    handler();
  };

  beforeAll(() => {
    amd.shim();
  });

  beforeEach(() => {
    // We have to do spy setup like mockReturnValue in a `beforeEach`. Otherwise
    // Jest will clear it out after the first run.
    workbenchModule.create.mockReturnValue({ dispose: workbenchDisposeSpy });

    amd.define<WorkbenchModule>('vs/workbench/workbench.web.main', () => workbenchModule);
    amd.define('vs/base/common/buffer', () => bufferModule);
  });

  afterEach(() => {
    amd.cleanup();
  });

  describe('startClientOnly', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let subject: any;

    beforeEach(() => {
      startClientOnly(CLIENT_ONLY_TEST_CONFIG);

      expect(workbenchModule.create).toHaveBeenCalledTimes(1);

      [[, subject]] = workbenchModule.create.mock.calls;
    });

    it('creates workbench on body', () => {
      expect(workbenchModule.create).toHaveBeenCalledWith(document.body, subject);
    });

    it('creates workbench with enabledExtensions', () => {
      const { publisher, name } = webIdeExtensionMeta;

      expect(subject).toMatchObject({
        enabledExtensions: [`${publisher}.${name}`],
      });
    });

    it('creates a workbench with a custom windowIndicator', () => {
      expect(subject).toMatchObject({
        windowIndicator: {
          label: '$(gitlab-tanuki) GitLab',
          command: 'gitlab-web-ide.open-remote-window',
        },
      });
    });

    it('creates workbench with trusted domains', () => {
      expect(subject).toMatchObject({
        additionalTrustedDomains: [
          'gitlab.com',
          'about.gitlab.com',
          'docs.gitlab.com',
          'test.example-gitlab.com',
          'test.example.com',
        ],
      });
    });

    it('has default layout', () => {
      expect(subject).toMatchObject({
        defaultLayout: {
          force: true,
          editors: [],
        },
      });
    });
  });

  describe('startClientOnly with filePath', () => {
    beforeEach(() => {
      startClientOnly({
        ...CLIENT_ONLY_TEST_CONFIG,
        filePath: '/foo/README.md',
      });
    });

    it('has default layout with filePath in editors', () => {
      expect(workbenchModule.create).toHaveBeenCalledWith(
        document.body,
        expect.objectContaining({
          defaultLayout: {
            force: true,
            editors: [{ uri: workbenchModule.URI.parse(`gitlab-web-ide:///lorem/foo/README.md`) }],
          },
        }),
      );
    });
  });

  describe('startRemote', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let subject: any;

    beforeEach(() => {
      startRemote(REMOTE_TEST_CONFIG);

      expect(workbenchModule.create).toHaveBeenCalledTimes(1);

      [[, subject]] = workbenchModule.create.mock.calls;
    });

    it('creates workbench on body', () => {
      expect(workbenchModule.create).toHaveBeenCalledWith(document.body, subject);
    });

    it('creates workbench without extra extensions', () => {
      expect(subject).not.toHaveProperty('enabledExtensions');
    });

    it('sets remote development properties', () => {
      expect(subject).toMatchObject({
        remoteAuthority: REMOTE_TEST_CONFIG.remoteAuthority,
        connectionToken: REMOTE_TEST_CONFIG.connectionToken,
      });
    });

    it('adds custom workspace provider', async () => {
      expect(subject.workspaceProvider.workspace.folderUri).toBe(
        `URI.from-vscode-remote-${REMOTE_TEST_CONFIG.hostPath}-${REMOTE_TEST_CONFIG.remoteAuthority}}`,
      );
      await expect(subject.workspaceProvider.open()).resolves.toBe(false);
    });

    it('handles onWillShutdown event', () => {
      expect(workbenchModule.events.onWillShutdown).toHaveBeenCalledWith(expect.any(Function));
    });

    // why: base assertion for other tests that postMessage is not called by default
    it('does not post any message', () => {
      expect(postMessage).not.toHaveBeenCalled();
    });

    // why: base assertion for other tests that dispose is not called by default
    it('workbench dispose should not be called', () => {
      expect(workbenchDisposeSpy).not.toHaveBeenCalled();
    });
  });

  describe('when onWillShutdown event is triggered', () => {
    beforeEach(() => {
      startRemote(REMOTE_TEST_CONFIG);
    });

    it('when onWillShutdown is triggered, posts close message', () => {
      triggerOnWillShutdown();

      expect(postMessage).toHaveBeenCalledWith({
        key: 'close',
      });
    });
  });

  describe('when startRemote raises an exception', () => {
    const error = new Error();

    beforeEach(() => {
      workbenchModule.create.mockImplementationOnce(() => {
        throw error;
      });
      startRemote(REMOTE_TEST_CONFIG);
    });

    it('posts START_WORKBENCH_FAILED error message', () => {
      expect(postMessage).toHaveBeenCalledWith({
        key: 'error',
        params: { errorType: ErrorType.START_WORKBENCH_FAILED },
      });
    });

    it('logs the workbench creation error', () => {
      expect(workbenchModule.logger.log).toHaveBeenCalledWith(expect.any(Number), String(error));
    });
  });
});
