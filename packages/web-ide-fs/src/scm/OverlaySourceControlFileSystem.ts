import { FileFlag } from 'browserfs/dist/node/core/file_flag';
import { IReadonlyPromisifiedBrowserFS } from '../browserfs/types';
import { convertToFileStats } from '../browserfs/utils';
import { IFileStats, ISourceControlFileSystem } from '../types';

export class OverlaySourceControlFileSystem implements ISourceControlFileSystem {
  private readonly _fs: IReadonlyPromisifiedBrowserFS;

  private readonly _originalFs: IReadonlyPromisifiedBrowserFS;

  constructor(fs: IReadonlyPromisifiedBrowserFS, originalFs: IReadonlyPromisifiedBrowserFS) {
    this._fs = fs;
    this._originalFs = originalFs;
  }

  async stat(path: string): Promise<IFileStats> {
    const stat = await this._fs.stat(path, false);

    return convertToFileStats(stat);
  }

  async statOriginal(path: string): Promise<IFileStats> {
    const stat = await this._originalFs.stat(path, false);

    return convertToFileStats(stat);
  }

  async readFile(path: string): Promise<Uint8Array> {
    // We know this will be a Buffer because we passed "null" for "utf-8"
    return <Buffer>await this._fs.readFile(path, null, FileFlag.getFileFlag('r'));
  }

  async readFileOriginal(path: string): Promise<Uint8Array> {
    // We know this will be a Buffer because we passed "null" for "utf-8"
    return <Buffer>await this._originalFs.readFile(path, null, FileFlag.getFileFlag('r'));
  }
}
