import { joinPaths } from '@gitlab/utils-path';
import { GraphQLClient } from 'graphql-request';
import { ResponseError } from './ResponseError';
import { gitlab } from './types';

import {
  getProjectUserPermissionsResult,
  getProjectUserPermissionsQuery,
  getProjectUserPermissionsVariables,
} from './graphql/getProjectUserPermissions.query';

const withParams = (baseUrl: string, params: Record<string, string>) => {
  const paramEntries = Object.entries(params);

  if (!paramEntries.length) {
    return baseUrl;
  }

  const url = new URL(baseUrl);

  paramEntries.forEach(([key, value]) => {
    url.searchParams.append(key, value);
  });

  return url.toString();
};

export interface IGitLabClientConfig {
  baseUrl: string;
  authToken: string;
  httpHeaders?: Record<string, string>;
}

export class GitLabClient {
  private readonly _baseUrl: string;

  private readonly _httpHeaders: Record<string, string>;

  private readonly _graphqlClient: GraphQLClient;

  constructor(config: IGitLabClientConfig) {
    this._baseUrl = config.baseUrl;
    this._httpHeaders = {
      ...(config.httpHeaders || {}),
      ...(config.authToken ? { 'PRIVATE-TOKEN': config.authToken } : {}),
    };

    const graphqlUrl = joinPaths(this._baseUrl, 'api', 'graphql');
    this._graphqlClient = new GraphQLClient(graphqlUrl, {
      headers: this._httpHeaders,
    });
  }

  async fetchProjectUserPermissions(projectPath: string) {
    const result = await this._graphqlClient.request<
      getProjectUserPermissionsResult,
      getProjectUserPermissionsVariables
    >(getProjectUserPermissionsQuery, {
      projectPath,
    });

    return result.project.userPermissions;
  }

  fetchProject(projectId: string): Promise<gitlab.Project> {
    const url = this._buildApiUrl('projects', projectId);

    return this._fetchGetJson(url);
  }

  fetchMergeRequest(projectId: string, mrId: string): Promise<gitlab.MergeRequest> {
    const url = this._buildApiUrl('projects', projectId, 'merge_requests', mrId);

    return this._fetchGetJson(url);
  }

  fetchProjectBranch(projectId: string, branchName: string): Promise<gitlab.Branch> {
    const url = this._buildApiUrl('projects', projectId, 'repository', 'branches', branchName);

    return this._fetchGetJson(url);
  }

  fetchTree(projectId: string, ref: string): Promise<gitlab.RepositoryTreeItem[]> {
    const url = this._buildApiUrl('projects', projectId, 'repository', 'tree');

    const params = {
      ref,
      recursive: 'true',
      pagination: 'none',
    };
    return this._fetchGetJson(url, params);
  }

  commit(projectId: string, payload: gitlab.CommitPayload): Promise<gitlab.Commit> {
    const url = this._buildApiUrl('projects', projectId, 'repository', 'commits');

    return this._fetchPostJson(url, payload);
  }

  async fetchFileRaw(projectId: string, ref: string, path: string): Promise<ArrayBuffer> {
    const url = this._buildApiUrl('projects', projectId, 'repository', 'files', path, 'raw');

    return this._fetchGetBuffer(url, { ref });
  }

  private _buildApiUrl(...parts: string[]) {
    return joinPaths(this._baseUrl, 'api', 'v4', ...parts.map(encodeURIComponent));
  }

  private async _fetchGetResponse(
    url: string,
    params: Record<string, string> = {},
  ): Promise<Response> {
    const response = await fetch(withParams(url, params), {
      method: 'GET',
      headers: this._httpHeaders,
    });

    if (response.status >= 400) {
      throw new ResponseError(`HTTP error: ${response.status}`, response);
    }

    return response;
  }

  private async _fetchPostJson<TResponse, TBody>(url: string, body: TBody): Promise<TResponse> {
    const response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        ...this._httpHeaders,
        'Content-Type': 'application/json',
      },
    });

    if (response.status >= 400) {
      throw new Error(`HTTP error: ${response.status}`);
    }

    return <Promise<TResponse>>response.json();
  }

  private async _fetchGetJson<T>(url: string, params: Record<string, string> = {}): Promise<T> {
    const response = await this._fetchGetResponse(url, params);

    return (await response.json()) as T;
  }

  private async _fetchGetBuffer(
    url: string,
    params: Record<string, string> = {},
  ): Promise<ArrayBuffer> {
    const response = await this._fetchGetResponse(url, params);

    return response.arrayBuffer();
  }
}
