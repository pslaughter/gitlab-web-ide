import * as vscode from 'vscode';
import checkoutBranch, { ISSUE_URL, ITEM_CHECKOUT_COMING_SOON } from './checkoutBranch';

describe('commands/checkoutBranch', () => {
  describe('default (when user cancels)', () => {
    beforeEach(async () => {
      jest.mocked(vscode.window.showQuickPick).mockResolvedValue(undefined);

      await checkoutBranch();
    });

    it('shows quick pick', () => {
      expect(vscode.window.showQuickPick).toHaveBeenCalledWith([ITEM_CHECKOUT_COMING_SOON], {
        canPickMany: false,
        ignoreFocusOut: false,
        matchOnDescription: false,
        matchOnDetail: false,
        placeHolder: 'Select a branch to check out',
      });
    });

    it('does not open anything', () => {
      expect(vscode.env.openExternal).not.toHaveBeenCalled();
    });
  });

  describe('when user selects item', () => {
    beforeEach(async () => {
      jest.mocked(vscode.window.showQuickPick).mockResolvedValue(ITEM_CHECKOUT_COMING_SOON);

      await checkoutBranch();
    });

    it('opens issue url', () => {
      expect(vscode.env.openExternal).toHaveBeenCalledWith(vscode.Uri.parse(ISSUE_URL));
    });
  });
});
