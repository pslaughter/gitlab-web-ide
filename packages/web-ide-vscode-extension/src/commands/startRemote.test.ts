import * as vscodeFacade from '../vscodeUi';
import startRemote from './startRemote';
import { startRemote as startRemoteMediator } from '../mediator';
import type { IInputResponse } from '../vscodeUi';

jest.mock('../mediator');
jest.mock('../vscodeUi');

describe('commands/startRemote', () => {
  const testRemoteHost = 'foobar.com';
  const testRemotePath = '/';
  const testConnectionToken = 'foobar';

  const mockShowInputBoxResponse = (response: IInputResponse<string>) => {
    jest.mocked(vscodeFacade.showInputBox).mockResolvedValueOnce(response);
  };
  const mockShowInputBoxCanceled = () => mockShowInputBoxResponse({ canceled: true });
  const mockShowInputBoxAccepted = (value: string) =>
    mockShowInputBoxResponse({ canceled: false, value });
  const getShowInputBoxOptions = (idx: number) =>
    jest.mocked(vscodeFacade.showInputBox).mock.calls[idx][0];
  const getShowInputBoxValidation = (idx: number) => getShowInputBoxOptions(idx)?.validate;

  beforeEach(() => {
    jest.mocked(vscodeFacade.showInputBox).mockResolvedValue({ canceled: true });
  });

  describe('on step 1', () => {
    beforeEach(async () => {
      await startRemote();
    });

    it('shows an input box requesting remote host', async () => {
      expect(getShowInputBoxOptions(0)).toEqual({
        placeholder: expect.any(String),
        ignoreFocusOut: true,
        title: expect.any(String),
        prompt: 'Remote host',
        step: 1,
        totalSteps: 3,
        validate: expect.any(Function),
      });
    });

    it.each`
      input                            | expected
      ${''}                            | ${expect.stringContaining('is required')}
      ${'https://test'}                | ${expect.stringContaining('is invalid')}
      ${'test.124test'}                | ${undefined}
      ${'remote-host.gitlab.com:3000'} | ${undefined}
    `('validation with input="$input"', async ({ input, expected }) => {
      const actual = await getShowInputBoxValidation(0)?.(input);

      expect(actual).toEqual(expected);
    });
  });

  describe('on step 2', () => {
    beforeEach(async () => {
      mockShowInputBoxAccepted(testRemoteHost);
      await startRemote();
    });

    it('shows an input box requesting remote path', async () => {
      expect(getShowInputBoxOptions(1)).toEqual({
        placeholder: expect.any(String),
        ignoreFocusOut: true,
        title: expect.any(String),
        prompt: `File path to repository on ${testRemoteHost}`,
        step: 2,
        totalSteps: 3,
        validate: expect.any(Function),
      });
    });

    it.each`
      input       | expected
      ${''}       | ${expect.stringContaining('is required')}
      ${'foobar'} | ${expect.stringContaining('start with `/`')}
      ${'/foo'}   | ${undefined}
    `('validation with input="$input"', async ({ input, expected }) => {
      const actual = await getShowInputBoxValidation(1)?.(input);

      expect(actual).toEqual(expected);
    });
  });

  describe('on step 3', () => {
    beforeEach(async () => {
      mockShowInputBoxAccepted(testRemoteHost);
      mockShowInputBoxAccepted(testRemotePath);

      await startRemote();
    });

    it('prompts for connectionToken', () => {
      expect(getShowInputBoxOptions(2)).toEqual({
        ignoreFocusOut: true,
        password: true,
        prompt: 'Connection token',
        step: 3,
        totalSteps: 3,
        title: 'Connect to a remote environment',
      });
    });
  });

  describe('when completed', () => {
    beforeEach(async () => {
      mockShowInputBoxAccepted(testRemoteHost);
      mockShowInputBoxAccepted(testRemotePath);
      mockShowInputBoxAccepted(testConnectionToken);

      await startRemote();
    });

    it('calls startRemoteMediator', () => {
      expect(startRemoteMediator).toHaveBeenCalledWith({
        connectionToken: testConnectionToken,
        remoteHost: testRemoteHost,
        remotePath: testRemotePath,
      });
    });
  });

  describe.each([1, 2, 3])('when canceled at step %s', step => {
    beforeEach(async () => {
      Array(step - 1)
        .fill(1)
        .forEach(() => {
          mockShowInputBoxAccepted('foo');
        });

      mockShowInputBoxCanceled();

      await startRemote();
    });

    it('does not trigger startRemote', () => {
      expect(startRemoteMediator).not.toHaveBeenCalled();
    });

    it(`only triggers showInputBox ${step} times`, () => {
      expect(vscodeFacade.showInputBox).toHaveBeenCalledTimes(step);
    });
  });
});
