import * as vscode from 'vscode';

// why: export for testing
export const ITEM_CHECKOUT_COMING_SOON = {
  alwaysShow: true,
  label: '$(warning) Not supported yet (Press Enter to open the issue on gitlab.com)',
};
export const ISSUE_URL = 'https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/72';

export default async () => {
  const selection = await vscode.window.showQuickPick([ITEM_CHECKOUT_COMING_SOON], {
    canPickMany: false,
    ignoreFocusOut: false,
    matchOnDescription: false,
    matchOnDetail: false,
    placeHolder: 'Select a branch to check out',
  });

  if (selection === ITEM_CHECKOUT_COMING_SOON) {
    await vscode.env.openExternal(vscode.Uri.parse(ISSUE_URL));
  }
};
