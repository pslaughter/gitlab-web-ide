import * as vscode from 'vscode';
import {
  COMMAND_START,
  COMMAND_FETCH_FILE_RAW,
  COMMAND_READY,
  COMMAND_START_REMOTE,
  COMMAND_COMMIT,
  COMMAND_PREVENT_UNLOAD,
  COMMAND_SET_HREF,
} from '@gitlab/vscode-mediator-commands';
import type {
  StartCommandResponse,
  VSCodeBuffer,
  StartRemoteMessageParams,
  PreventUnloadMessageParams,
  GitLabCommitPayload,
  StartCommandOptions,
} from '@gitlab/vscode-mediator-commands';

export const start = (options: StartCommandOptions = {}): Thenable<StartCommandResponse> =>
  vscode.commands.executeCommand(COMMAND_START, options);

export const fetchFileRaw = (ref: string, path: string): Thenable<VSCodeBuffer> =>
  vscode.commands.executeCommand(COMMAND_FETCH_FILE_RAW, ref, path);

export const ready = () => vscode.commands.executeCommand(COMMAND_READY);

export const startRemote = (params: StartRemoteMessageParams) =>
  vscode.commands.executeCommand(COMMAND_START_REMOTE, params);

export const commit = (payload: GitLabCommitPayload) =>
  vscode.commands.executeCommand(COMMAND_COMMIT, payload);

export const preventUnload = (params: PreventUnloadMessageParams) =>
  vscode.commands.executeCommand(COMMAND_PREVENT_UNLOAD, params);

export const setHref = (href: string) => vscode.commands.executeCommand(COMMAND_SET_HREF, href);
