// TODO: For some reason `ts-jest` isn't finsing the `.d.ts` files
import '../../vscode.proposed.scmActionButton.d';

import * as vscode from 'vscode';
import { FileStatusType, IFileStatus } from '@gitlab/web-ide-fs';
import { createSourceControlViewModel } from './create';
import { ISourceControlViewModel, IStatusViewModel } from './types';
import {
  COMMIT_COMMAND_ID,
  COMMIT_COMMAND_TEXT,
  SOURCE_CONTROL_CHANGES_ID,
  SOURCE_CONTROL_CHANGES_NAME,
  SOURCE_CONTROL_ID,
  SOURCE_CONTROL_NAME,
} from '../constants';
import { ResourceDecorationProvider } from './ResourceDecorationProvider';
import { createStatusViewModel, toResourceState } from './status';

jest.mock('./ResourceDecorationProvider');

const TEST_REPO_ROOT = '/test-repo-root';
const TEST_NEW_COMMIT_MESSAGE = 'New commit message from a test!';
const TEST_NEW_STATUS: IFileStatus[] = [{ type: FileStatusType.Deleted, path: '/README.md' }];

describe('scm/create', () => {
  let disposables: vscode.Disposable[];
  let subject: ISourceControlViewModel;
  let sourceControl: vscode.SourceControl;
  let changesGroup: vscode.SourceControlResourceGroup;
  let decorationProvider: vscode.FileDecorationProvider;

  const getResourceDecorationProviderInstance = () =>
    jest.mocked(ResourceDecorationProvider).mock.instances[0];

  const createMockChangesGroup = (id: string): vscode.SourceControlResourceGroup => ({
    id,
    label: '',
    resourceStates: [],
    hideWhenEmpty: false,
    dispose: jest.fn(),
  });

  const createMockSourceControl = (id: string): vscode.SourceControl => ({
    id,
    createResourceGroup: jest.fn().mockImplementation((resourceGroupId: string) => {
      changesGroup = createMockChangesGroup(resourceGroupId);

      return changesGroup;
    }),
    dispose: jest.fn(),
    inputBox: {
      enabled: true,
      placeholder: '',
      value: '',
      visible: true,
    },
    label: '',
    rootUri: undefined,
  });

  beforeEach(() => {
    disposables = [];

    jest.mocked(vscode.scm.createSourceControl).mockImplementation((id: string) => {
      sourceControl = createMockSourceControl(id);

      return sourceControl;
    });
    jest
      .mocked(ResourceDecorationProvider.prototype.createVSCodeDecorationProvider)
      .mockReturnValue(decorationProvider);

    subject = createSourceControlViewModel(disposables, TEST_REPO_ROOT);
  });

  it('creates sourceControl', () => {
    expect(vscode.scm.createSourceControl).toHaveBeenCalledWith(
      SOURCE_CONTROL_ID,
      SOURCE_CONTROL_NAME,
    );

    expect(sourceControl).toMatchObject({
      id: SOURCE_CONTROL_ID,
      acceptInputCommand: {
        command: COMMIT_COMMAND_ID,
        title: COMMIT_COMMAND_TEXT,
        arguments: [],
      },
      inputBox: {
        enabled: true,
        placeholder: 'Commit message',
      },
      actionButton: {
        description: COMMIT_COMMAND_TEXT,
        enabled: true,
        secondaryCommands: [],
        command: {
          title: COMMIT_COMMAND_TEXT,
          command: COMMIT_COMMAND_ID,
        },
      },
    });
  });

  it('creates resourceGroup', () => {
    expect(sourceControl.createResourceGroup).toHaveBeenCalledWith(
      SOURCE_CONTROL_CHANGES_ID,
      SOURCE_CONTROL_CHANGES_NAME,
    );

    expect(changesGroup).toMatchObject({
      id: SOURCE_CONTROL_CHANGES_ID,
      hideWhenEmpty: false,
      resourceStates: [],
    });
  });

  it('creates ResourceDecorationoProvider', () => {
    expect(ResourceDecorationProvider).toHaveBeenCalled();
  });

  it('register file decoration provider from ResourceDecorationProvider', () => {
    expect(vscode.window.registerFileDecorationProvider).toHaveBeenCalledWith(decorationProvider);
  });

  describe('default', () => {
    it('getStatus - returns empty status', () => {
      expect(subject.getStatus()).toEqual([]);
    });

    it('getCommitMessage - returns empty commit message', () => {
      expect(subject.getCommitMessage()).toEqual('');
    });
  });

  describe('when commit message changes', () => {
    beforeEach(() => {
      sourceControl.inputBox.value = TEST_NEW_COMMIT_MESSAGE;
    });

    it('getCommitMessage - returns new commit message', () => {
      expect(subject.getCommitMessage()).toEqual(TEST_NEW_COMMIT_MESSAGE);
    });
  });

  describe('when updated with new status', () => {
    let statusVms: IStatusViewModel[];

    beforeEach(() => {
      subject.update(TEST_NEW_STATUS);
      statusVms = TEST_NEW_STATUS.map(x => createStatusViewModel(x, TEST_REPO_ROOT));
    });

    it('getStatus - returns new status', () => {
      expect(subject.getStatus()).toBe(TEST_NEW_STATUS);
    });

    it('updates the resource decoration provider', () => {
      expect(getResourceDecorationProviderInstance().update).toHaveBeenCalledWith(statusVms);
    });

    it('update changeGroup resource states', () => {
      expect(changesGroup.resourceStates).toEqual(statusVms.map(toResourceState));
    });
  });
});
