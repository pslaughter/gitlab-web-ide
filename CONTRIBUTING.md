# Contributing

Thanks for your interest in contributing to the `@gitlab/web-ide` project!

You can start contributing by following the instructions in the
[Development Environment Setup](./docs/dev/development_environment_setup.md). Then, be sure to familiarize yourself with the
[Style Guide](./docs/dev/style_guide.md).
