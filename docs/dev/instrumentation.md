# Instrumentation

[TOC]

The Web IDE sends tracking events to [GitLab snowplow service](https://docs.gitlab.com/ee/development/snowplow/implementation.html)
by exposing a `handleTracking` function in the [web-ide package](./architecture_packages.md#web-ide).

```typescript
import { start } from '@gitlab/web-ide';

start({
  handleTracking: (trackingEvent: TrackingEvent) {
    // Invoke snowplow service
  }
})
```

## Tracking sources

Tracking events have two sources: The [VSCode fork](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/) and
the [Web IDE VSCode extension](./architecture_packages.md#web-ide-vscode-extension). These packages send tracking messages
that contain one or more tracking events.

The Web IDE sents these events from inside the iframe to the
container window using the `postMessage` api.

### Web IDE VSCode extension

The Web IDE sends tracking events using the `trackEvent` mediator command.

```mermaid
sequenceDiagram
    participant A as web-ide-vscode-extension
    participant B as vscode-mediator-commands
    participant C as @gitlab/web-ide
    participant D as GitLab application
    A->>+B: trackEvent(event)
    B->>-C: postMessage('web-ide-tracking', event)
    Note right of A: iframe
    Note right of C: container window
    activate C
    C->>+D: handleTracking(event)
    deactivate C
```

### VSCode fork

The Web IDE re-routes logs produced by the
[VSCode fork TelemetryService](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/blob/main/src/vs/workbench/services/telemetry/browser/telemetryService.ts#L27)
to the GitLab application. The Web IDE translates the event names used
by VSCode into [`TrackingEvent`](../../packages/web-ide-types/src/tracking.ts) instances. If a VSCode’s tracking event doesn’t have an equivalent `TrackingEvent` type, the Web IDE will ignore it.

```mermaid
sequenceDiagram
    participant A as vscode-fork
    participant B as @gitlab/web-ide
    participant C as GitLab application
    activate B
    A->>B: postMessage('vscode-tracking', event)
    Note right of A: iframe
    Note right of B: container window
    B->>+C: handleTracking · mapToTrackingEvent(event)
    deactivate B
```
